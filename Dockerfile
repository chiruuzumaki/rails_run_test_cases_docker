# See https://intercityup.com/blog/how-i-build-a-docker-image-for-my-rails-app.html
# See https://intercityup.com/blog/deploy-rails-app-including-database-configuration-env-vars-assets-using-docker.html
FROM ubuntu

RUN apt update
RUN apt install -y sudo
RUN apt-get install -y tzdata
RUN sudo apt-get install -y build-essential libcurl4-openssl-dev redis-server tzdata

RUN mkdir /root/webapp

# Add startup script to run during container startup

RUN apt-get install -y git curl libssl-dev libreadline-dev zlib1g-dev imagemagick libmagickcore-dev libmagickwand-dev libpq-dev postgresql 
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg |  apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
RUN sudo apt-get update && sudo apt-get install -y yarn

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN git clone https://github.com/rbenv/rbenv.git  /root/.rbenv

RUN git clone https://github.com/rbenv/ruby-build.git /root/.rbenv/plugins/ruby-build

RUN /root/.rbenv/plugins/ruby-build/install.sh
COPY pg_hba.conf /etc/postgresql/10/main/

ENV PATH /root/.rbenv/bin:$PATH
RUN echo 'eval "$(rbenv init -)"' >> ~/.bashrc

RUN echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh # or /etc/profile

COPY   run.sh /root/
RUN rbenv install 2.6.1 && rbenv install 2.5.3
RUN  chmod +x /root/run.sh
CMD  /root/run.sh
