service postgresql start 
service redis-server start
cd /root/webapp && cat .ruby-version | rbenv install 
cd /root/webapp && cat .ruby-version | rbenv global && rbenv rehash
cd /root/webapp &&  /root/.rbenv/shims/gem install bundler
cd /root/webapp &&  /root/.rbenv/shims/bundle install && /root/.rbenv/shims/bundle exec rake db:drop RAILS_ENV=test && /root/.rbenv/shims/bundle exec rake db:create RAILS_ENV=test &&  /root/.rbenv/shims/bundle exec rake db:migrate RAILS_ENV=test &&  /root/.rbenv/shims/bundle exec rails db:environment:set RAILS_ENV=test &&  /root/.rbenv/shims/bundle exec rake db:seed RAILS_ENV=test &&  /root/.rbenv/shims/bundle exec rake db:schema:load RAILS_ENV=test &&  /root/.rbenv/shims/bundle exec rspec spec
